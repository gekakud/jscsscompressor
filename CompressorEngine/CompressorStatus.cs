namespace FileCompressor
{
    public enum CompressorStatus
    {
        Done,
        NoFilesFound,
        Error
    }
}