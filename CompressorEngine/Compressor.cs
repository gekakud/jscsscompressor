﻿using System;
using System.Collections.Generic;
using System.IO;
using Yahoo.Yui.Compressor;

namespace FileCompressor
{
    public class Compressor
    {
        private readonly JavaScriptCompressor _jsCompressor;
        private readonly CssCompressor _cssCompressor;

        #region Constructor
        public Compressor()
        {
            _jsCompressor = new JavaScriptCompressor
            {
                CompressionType = CompressionType.Standard,
                Encoding = System.Text.Encoding.UTF8,
                DisableOptimizations = false
            };

            _cssCompressor = new CssCompressor
            {
                LineBreakPosition = 8000,
                CompressionType = CompressionType.Standard,
                RemoveComments = true
            };
        } 
        #endregion

        public CompressorStatus CompressFilesIn(string p_srcFolder, CompressType p_type, bool p_onefile)
        {
            if (p_srcFolder.Equals(string.Empty) || !Directory.Exists(p_srcFolder))
                return CompressorStatus.Error;

            var notCompressedFiles = SearchFilesWithExtension(p_srcFolder,
                p_type.Equals(CompressType.Js) ? ".js" : ".css");
            if (notCompressedFiles.Count < 1)
            {
                Console.WriteLine("No files found");
                return CompressorStatus.NoFilesFound;
            }

            foreach (var file in notCompressedFiles)
            {
                Console.WriteLine(file.OriginalFile + " size:" + file.OriginalSize);
                CompressMe(file, p_type);
            }

            return CompressorStatus.Done;
        }

        public List<SomeFile> SearchFilesWithExtension(string p_path, string p_ext)
        {
            var filesList = new List<SomeFile>();
            var files = Directory.GetFiles(p_path, "*" + p_ext, SearchOption.AllDirectories);

            var destFolder = p_path + "_min\\";
            if (!Directory.Exists(destFolder))
            {
                Directory.CreateDirectory(destFolder);
            }

            foreach (var file in files)
            {
                filesList.Add(new SomeFile
                {
                    OriginalFile = file,
                    CompressedFile = destFolder + Path.GetFileName(file)
                });
            }

            return filesList;
        }

        private async void CompressMe(SomeFile p_file, CompressType p_type)
        {
            try
            {
                var dataReader = File.OpenText(p_file.OriginalFile);
                var data = await dataReader.ReadToEndAsync();
                dataReader.Close();

                data = p_type.Equals(CompressType.Js) ? _jsCompressor.Compress(data) : _cssCompressor.Compress(data);

                var dataWriter = File.CreateText(p_file.CompressedFile);
                dataWriter.Write(data);
                await dataWriter.FlushAsync();
                dataWriter.Close();
            }
            catch (Exception e)
            {
                throw new Exception("Something went wrong in CompressMe method"+e.StackTrace+e.Message);
            }
        }
    }
}