﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileCompressor
{
    public class SomeFile
    {
        #region public properties
        public string OriginalFile { get; set; }
        public string CompressedFile { get; set; }

        public long OriginalSize
        {
            get { return OriginalFile != null ? new FileInfo(OriginalFile).Length : 0; }
        }

        public long CompressedSize
        {
            get
            {
                return CompressedFile != null ? new FileInfo(CompressedFile).Length : 0;
            }
        }

        public long Ratio
        {
            get
            {
                if (OriginalSize > 0 && CompressedSize > 0)
                    return CompressedSize / (OriginalSize / 100);
                return -1;
            }
        }
        #endregion
    }
}
