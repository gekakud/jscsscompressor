﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using CompressorGUI.DataModel;
using FileCompressor;
using GalaSoft.MvvmLight.Command;


namespace CompressorGUI
{
    public class ViewModel : INotifyPropertyChanged
    {
        private List<string> _list;
        private string _compressFolderPath;
        private CompressorClass _compressor;

        public List<string> ListOfFiles
        {
            get { return _list; }
            set
            {
                _list = value;
                PropertyChanged(this,new PropertyChangedEventArgs("ListOfFiles"));
            }
        }

        public string CompressFolderPath
        {
            get { return _compressFolderPath; }
            set
            {
                _compressFolderPath = value;
                ListOfFiles = _compressor.GetListOfCompatibleFiles(value, CompressType.All);
                PropertyChanged(this, new PropertyChangedEventArgs("CompressFolderPath"));
            }
        }

        public RelayCommand OpenFolderDialog { get; private set; }
        public RelayCommand CompressButtonClicked { get; private set; }

        public ViewModel()
        {
            OpenFolderDialog = new RelayCommand(OpenFolderDialogInternal);
            CompressButtonClicked = new RelayCommand(CompressClickInternal);

            _compressor = CompressorClass.Instance;
            _compressFolderPath = "";
        }

        private void CompressClickInternal()
        {
            //compress here
            switch (_compressor.CompressMe(_compressFolderPath, CompressType.Js))
            {
                case CompressorStatus.Done:
                    Console.WriteLine(CompressorStatus.Done.ToString());
                    break;

                case CompressorStatus.Error:
                    Console.WriteLine(CompressorStatus.Error.ToString());
                    break;

                case CompressorStatus.NoFilesFound:
                    Console.WriteLine(CompressorStatus.NoFilesFound.ToString());
                    break;
            }
        }

        private void OpenFolderDialogInternal()
        {
            CompressFolderPath = InputDataLoader.GetInputDataFolder();
            
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        protected virtual void OnPropertyChanged([CallerMemberName] string p_propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(p_propertyName));
        }
    }
}