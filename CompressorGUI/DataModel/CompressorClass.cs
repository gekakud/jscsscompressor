﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FileCompressor;

namespace CompressorGUI.DataModel
{
    
    public class CompressorClass
    {
        private static readonly CompressorClass instance = new CompressorClass();
        private Compressor _compressorEngine;

        public CompressorClass()
        {
            _compressorEngine = new Compressor();
            //some init stuff
        }

        public static CompressorClass Instance
        {
            get { return instance; }
        }

        public List<string> GetListOfCompatibleFiles(string p_path,CompressType p_type)
        {
            //todo: CompressType check shoul be done in separate function!!!
            var jslst = _compressorEngine.SearchFilesWithExtension(p_path, ".js");
            
            var csslst = _compressorEngine.SearchFilesWithExtension(p_path, ".css");
            
            csslst.AddRange(jslst);

            var filesList = new List<string>();

            if (csslst.Count > 0)
            {
                filesList.AddRange(csslst.Select(file => file.OriginalFile));
            }

            return filesList;
        } 

        public CompressorStatus CompressMe(string p_path,CompressType p_type)
        {
            return _compressorEngine.CompressFilesIn(p_path, p_type, false);
        }
    }
}
