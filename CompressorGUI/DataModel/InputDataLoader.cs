﻿using System.Windows.Forms;

namespace CompressorGUI
{
    public static class InputDataLoader
    {
        public static string GetInputDataFolder()
        {
            var dialog = new FolderBrowserDialog();
            var result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {      
                var folderName = dialog.SelectedPath;
                return folderName;
            }

            return string.Empty;
        }
    }
}
